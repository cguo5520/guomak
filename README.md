# guomak
Modified Colemak Linux for Developers

## Linux
To install:
```
cd guomak
sudo bash INSTALL.sh

# Restart your computer
# Go to settings => languages
# Add guomak
# Remove colemak. Other layouts seem to cause problems with guomak
```

To uninstall:
```
cd guomak
sudo bash UNINSTALL.sh
```

## Mac
Generate karabiner.json with `cd mac; node karabiner.generator`
Install karabiner, then move the karabiner.json file to ~/.config/karabiner/karabiner.json

## Windows
Install AHK

## TODO:
Unit tests with test files
Installing and then uninstalling creates empty lines
Polish up the mac setup. Probably need to add level2?

Ideally, we should be testing via Dockerfile
xkbcomp to compile the file
xev to get the output
not sure how to simulate keypresses